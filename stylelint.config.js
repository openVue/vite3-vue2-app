// https://juejin.cn/post/7156168274529484830#heading-41
// https://juejin.cn/post/7163836330241753124#heading-15
module.exports = {
  // stylelint - Stylelint 本体
  // stylelint-less - Stylelint Less 规则
  // stylelint-config-prettier - 关闭 Stylelint 中与 Prettier 中会发生冲突的规则
  // stylelint-config-standard - Stylelint 官方推荐规则
  // stylelint-config-recess-order - 对 CSS 声明进行排序
  // stylelint-order - CSS 属性顺序规则插件
  root: true,
  defaultSeverity: 'error',
  plugins: ['stylelint-order', 'stylelint-less'], // CSS 属性顺序规则插件
  extends: [
    'stylelint-config-standard', // stylelint标准配置
    'stylelint-config-html/html', // html相关配置
    'stylelint-config-html/vue', //  vue相关配置
    'stylelint-config-recess-order', // 对CSS声明进行排序
    'stylelint-config-prettier' // 关闭Stylelint中与Prettier中会发生冲突的规则
  ],
  rules: {
    // 禁止在覆盖高特异性选择器之后出现低特异性选择器
    'no-descending-specificity': null,
    // 禁止空源码
    'no-empty-source': null,
    // 禁止字体族中缺少泛型族关键字
    'font-family-no-missing-generic-family-keyword': null,
    // 禁止未知的@规则
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'tailwind',
          'apply',
          'variants',
          'responsive',
          'screen',
          'function',
          'if',
          'each',
          'include',
          'mixin'
        ]
      }
    ],
    // 不允许未知函数
    'function-no-unknown': null,
    // 不允许未知单位
    'unit-no-unknown': [true, { ignoreUnits: ['rpx'] }],
    // 标记 CSS 规范中未知属性值
    'declaration-property-value-no-unknown': true,
    // 不允许选择器使用供应商前缀
    'selector-no-vendor-prefix': null,
    // 指定关键帧名称的模式
    'keyframes-name-pattern': null,
    // 指定类选择器的模式
    'selector-class-pattern': null,
    // 不允许值使用供应商前缀
    'value-no-vendor-prefix': null,
    // 要求或禁止在规则之前的空行
    'rule-empty-line-before': ['always', { ignore: ['after-comment', 'first-nested'] }]
  },
  ignoreFiles: ['**/*.js', '**/*.jsx', '**/*.tsx', '**/*.ts', '**/*.json', '**/*.md', '**/*.yaml'],
  overrides: [
    {
      files: ['*.vue', '**/*.vue', '*.html', '**/*.html'],
      customSyntax: 'postcss-html',
      rules: {
        // 禁止未知的伪类选择器
        'selector-pseudo-class-no-unknown': [true, { ignorePseudoClasses: ['deep', 'global'] }],
        // 禁止未知的伪元素选择器
        'selector-pseudo-element-no-unknown': [true, { ignorePseudoElements: ['v-deep', 'v-global', 'v-slotted'] }]
      }
    },
    {
      files: ['*.less', '**/*.less'],
      customSyntax: 'postcss-less',
      rules: {
        'less/color-no-invalid-hex': true,
        'less/no-duplicate-variables': true
      }
    }
  ]
}
