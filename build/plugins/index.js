import vue2 from '@vitejs/plugin-vue2'
import unplugin from './unplugin'
import mock from './mock'
import visualizer from './visualizer'
import compression from './compression'
import inspect from './inspect'
import legacy from './legacy'
import commonjs from './require'
import progress from './progress'
/**
 * vite插件
 * @param viteEnv - 环境变量配置
 */
export default function createVitePlugins(viteEnv, isBuild = false) {
  const vitePlugins = [vue2(), mock(viteEnv), ...unplugin(), commonjs()]
  if (isBuild) {
    vitePlugins.push(compression(), visualizer(), legacy(), progress())
  } else {
    vitePlugins.push(...inspect())
  }
  return vitePlugins
}
