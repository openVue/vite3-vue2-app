import Vue from 'vue'
import App from './App.vue'
import store from './store' // vuex
import router from './router' // 路由
import './styles/index.less' // 全局样式
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
